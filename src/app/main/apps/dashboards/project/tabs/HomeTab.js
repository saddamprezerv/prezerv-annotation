import { useSelector } from "react-redux";
import { motion } from "framer-motion";
import { selectWidgets } from "../store/widgetsSlice";
import Widget1 from "../widgets/Widget1";
import Widget2 from "../widgets/Widget2";
import Widget3 from "../widgets/Widget3";
import Widget4 from "../widgets/Widget4";
import Widget5 from "../widgets/Widget5";
import Widget6 from "../widgets/Widget6";
import Widget7 from "../widgets/Widget7";
import Widget1a from "../widgets/Widget1a";
import Widget2a from "../widgets/Widget2a";
import Widget3a from "../widgets/Widget3a";
import Widget4a from "../widgets/Widget4a";
import { DataContext } from "../../../../../../storeMain/context/data/DataContext";

const HomeTab1 = (props) => {
  const widgets = useSelector(selectWidgets);

  const container = {
    show: {
      transition: {
        staggerChildren: 0.1,
      },
    },
  };

  const item = {
    hidden: { opacity: 0, y: 20 },
    show: { opacity: 1, y: 0 },
  };

  const dueTaskCardHandler = () => {
    props.taskClickHandler(true, "dueTask");
    console.log("dueTaskCardHandler");
  };

  const overDueTaskCardHandler = () => {
    props.taskClickHandler(true, "overDueTask");
    console.log("dueTaskCardHandler");
  };

  const qaTaskCardHandler = () => {
    props.taskClickHandler(true, "qaTask");
    console.log("dueTaskCardHandler");
  };

  const dueTaskQACardHandler = () => {
    props.taskClickHandler(true, "dueTaskQA");
  };
  const overDueTaskCardQAHandler = () => {
    props.taskClickHandler(true, "overDueTask");
    console.log("dueTaskCardHandler");
  };

  const qaTaskCardQAHandler = () => {
    props.taskClickHandler(true, "qaTask");
    console.log("dueTaskCardHandler");
  };

  return (
    <motion.div
      className="flex flex-wrap"
      variants={container}
      initial="hidden"
      animate="show"
    >
      {props.data.userData.role === "Annotator" && (
        <>
          <motion.div
            variants={item}
            className="widget flex w-full sm:w-1/2 md:w-1/3 p-12"
            onClick={dueTaskCardHandler}
            style={{ cursor: "pointer" }}
          >
            <Widget1
              widget={widgets.widget1}
              dbValue={
                props.payLoadCheck.gridData ? props.data.gridCountTasks : 0
              }
            />
          </motion.div>

          <motion.div
            variants={item}
            className="widget flex w-full sm:w-1/2 md:w-1/3 p-12"
            onClick={overDueTaskCardHandler}
            style={{ cursor: "pointer" }}
          >
            <Widget2
              widget={widgets.widget2}
              dbValue={
                props.payLoadCheck.gridOverDue ? props.data.gridCountOverDue : 0
              }
            />
          </motion.div>
          <motion.div
            variants={item}
            className="widget flex w-full sm:w-1/2 md:w-1/3 p-12"
            onClick={qaTaskCardHandler}
            style={{ cursor: "pointer" }}
          >
            <Widget3
              widget={widgets.widget3}
              dbValue={props.payLoadCheck.gridQa ? props.data.gridCountQa : 0}
            />
          </motion.div>
        </>
      )}

      {props.data.userData.role === "QA" && (
        <>
          <motion.div
            variants={item}
            className="widget flex w-full sm:w-1/2 md:w-1/3 p-12"
            onClick={dueTaskQACardHandler}
            style={{ cursor: "pointer" }}
          >
            <Widget1
              widget={widgets.widget1}
              dbValue={
                props.payLoadCheck.gridData ? props.data.gridCountTasks : 0
              }
            />
          </motion.div>

          <motion.div
            variants={item}
            className="widget flex w-full sm:w-1/2 md:w-1/3 p-12"
            onClick={overDueTaskCardQAHandler}
            style={{ cursor: "pointer" }}
          >
            <Widget2
              widget={widgets.widget2}
              dbValue={
                props.payLoadCheck.gridOverDue ? props.data.gridCountOverDue : 0
              }
            />
          </motion.div>
          <motion.div
            variants={item}
            className="widget flex w-full sm:w-1/2 md:w-1/3 p-12"
            onClick={qaTaskCardQAHandler}
            style={{ cursor: "pointer" }}
          >
            <Widget3
              widget={widgets.widget3}
              dbValue={props.payLoadCheck.gridQa ? props.data.gridCountQa : 0}
            />
          </motion.div>
        </>
      )}

      {/* <motion.div
        variants={item}
        className="widget flex w-full sm:w-1/2 md:w-1/4 p-12"
      >
        <Widget4 widget={widgets.widget4} />
      </motion.div>
      <motion.div
        variants={item}
        className="widget flex w-full sm:w-1/2 md:w-1/4 p-12"
      >
        <Widget1a widget={widgets.widget1} />
      </motion.div>
      <motion.div
        variants={item}
        className="widget flex w-full sm:w-1/2 md:w-1/4 p-12"
      >
        <Widget2a widget={widgets.widget2} />
      </motion.div>
      <motion.div
        variants={item}
        className="widget flex w-full sm:w-1/2 md:w-1/4 p-12"
      >
        <Widget3a widget={widgets.widget3} />
      </motion.div>
      <motion.div
        variants={item}
        className="widget flex w-full sm:w-1/2 md:w-1/4 p-12"
      >
        <Widget4a widget={widgets.widget4} />
      </motion.div>
      <motion.div variants={item} className="widget flex w-full p-12">
        <Widget5 widget={widgets.widget5} />
      </motion.div> */}
      {/* <motion.div variants={item} className="widget flex w-full sm:w-1/2 p-12">
        <Widget6 widget={widgets.widget6} />
      </motion.div>
      <motion.div variants={item} className="widget flex w-full sm:w-1/2 p-12">
        <Widget7 widget={widgets.widget7} />
      </motion.div> */}
      {/* <motion.div variants={item} className="widget flex w-full p-12">
        <Widget5 widget={widgets.widget5} />
      </motion.div>
      <motion.div variants={item} className="widget flex w-full sm:w-1/2 p-12">
        <Widget6 widget={widgets.widget6} />
      </motion.div>
      <motion.div variants={item} className="widget flex w-full sm:w-1/2 p-12">
        <Widget7 widget={widgets.widget7} />
      </motion.div> */}
    </motion.div>
  );
};

//export default HomeTab;
const HomeTab = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <HomeTab1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default HomeTab;
