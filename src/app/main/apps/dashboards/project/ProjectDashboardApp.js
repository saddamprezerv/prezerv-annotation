import FusePageSimple from "@fuse/core/FusePageSimple";
import { styled } from "@mui/material/styles";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import withReducer from "app/store/withReducer";
import _ from "@lodash";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Box from "@mui/material/Box";
import ProjectDashboardAppHeader from "./ProjectDashboardAppHeader";
import ProjectDashboardAppHeaderMap from "./ProjectDashboardAppHeaderMap";
import ProjectDashboardAppSidebar from "./ProjectDashboardAppSidebar";
import reducer from "./store";
import { getWidgets, selectWidgets } from "./store/widgetsSlice";
import BudgetSummaryTab from "./tabs/BudgetSummaryTab";
import HomeTab from "./tabs/HomeTab";
import TeamMembersTab from "./tabs/TeamMembersTab";
import { DataContext } from "../../../../../storeMain/context/data/DataContext";
// import MapComp from "./mapComp/MapComp";
import Orders from "../../e-commerce/orders/Orders";

const Root = styled(FusePageSimple)(({ theme }) => ({
  "& .FusePageSimple-header": {
    minHeight: 160,
    height: 160,
    [theme.breakpoints.up("lg")]: {
      marginRight: 12,
      borderBottomRightRadius: 20,
    },
  },
  "& .FusePageSimple-toolbar": {
    minHeight: 56,
    height: 56,
    alignItems: "flex-end",
  },
  "& .FusePageSimple-rightSidebar": {
    width: 288,
    border: 0,
    padding: "12px 0",
  },
  "& .FusePageSimple-content": {
    maxHeight: "100%",
    "& canvas": {
      maxHeight: "100%",
    },
  },
}));

const ProjectDashboardApp1 = (props) => {
  const dispatch = useDispatch();
  const widgets = useSelector(selectWidgets);

  const pageLayout = useRef(null);
  const [tabValue, setTabValue] = useState(0);

  useEffect(() => {
    dispatch(getWidgets());
  }, [dispatch]);

  if (_.isEmpty(widgets)) {
    return null;
  }

  return (
    <>
      {props.ui.mapPage.show === false && (
        <Root
          header={<ProjectDashboardAppHeader pageLayout={pageLayout} />}
          content={
            <div className="p-12 lg:ltr:pr-0 lg:rtl:pl-0">
              <HomeTab />
            </div>
          }
          // rightSidebarContent={<ProjectDashboardAppSidebar />}
          ref={pageLayout}
        />
      )}
      {props.ui.mapPage.show && <Orders />}
    </>
  );
};

const ProjectDashboardApp = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <ProjectDashboardApp1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default withReducer("projectDashboardApp", reducer)(ProjectDashboardApp);
