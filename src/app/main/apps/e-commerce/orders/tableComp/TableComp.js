import React from "react";
import styles from "./TableComp.module.css";
import { motion } from "framer-motion";
import { DataContext } from "../../../../../../storeMain/context/data/DataContext";
import Table from "./table/Table";

const TableComp1 = (props) => {
  const rowClicked = (projId, currentStatus) => {
    console.log("currentStatuscurrentStatus", currentStatus);
    let eventName = "start";
    // if (currentStatus === "Not Started") {
    //   eventName = "start";
    // } else
    if (currentStatus === "Upload Project") {
      eventName = "check_qa";
    }
    if (currentStatus === "Rejection") {
      eventName = "correction";
    }
    console.log(projId, props.data.userData.user_id);
    props.getProjectDownload(
      projId,
      props.data.userData.user_id,
      eventName,
      currentStatus
    );
    props.getHistory();
  };
  const container = {
    show: {
      transition: {
        staggerChildren: 0.1,
      },
    },
  };
  return (
    <motion.div
      className="flex flex-wrap"
      variants={container}
      initial="hidden"
      animate="show"
    >
      <motion.div
        className="widget flex w-full p-5"
        // style={{ backgroundColor: "blue" }}
      >
        <Table data={props.data} clicked={rowClicked} />
      </motion.div>
    </motion.div>
  );
};

const TableComp = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <TableComp1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default TableComp;
