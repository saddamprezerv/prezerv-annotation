import React from "react";
import styles from "./Table.module.css";
import Grid from "@mui/material/Grid";
import _ from "@lodash";

const Table = (props) => {
  const areaProjectsList = props.data.areaProjects;
  const projectStatus = props.data.projectStatus;
  return (
    <div className={styles.main}>
      <Grid container direction="row" spacing={1}>
        <div className={styles.headingHistory}>Grid Projects List</div>
      </Grid>
      <Grid container direction="row" spacing={1}>
        <div className={styles.flexConatiner}>
          <div className={styles.headingRow}>ID</div>
          <div className={styles.headingRow}>Project Name</div>
          <div className={styles.headingRow}>Processed Status</div>
        </div>
      </Grid>
      {areaProjectsList.map((k, i) => {
        const status = projectStatus.find(({ id }) => id === k.is_processed);
        //console.log(status);
        return (
          <Grid
            container
            direction="row"
            spacing={1}
            key={"projects" + k.name + i}
          >
            <div
              className={styles.flexConatiner}
              onClick={() => {
                props.clicked(k.id, status.name);
              }}
            >
              <div className={styles.headingValue}>{k.id}</div>
              <div className={styles.headingValue}>{k.name}</div>
              <div
                className={styles.headingValueHighLighted}
                style={{ backgroundColor: status.color }}
              >
                {status.name}
              </div>
              {/* {<div className={styles.headingValue}>{status}</div>} */}
            </div>
          </Grid>
        );
      })}
    </div>
  );
};

export default Table;
