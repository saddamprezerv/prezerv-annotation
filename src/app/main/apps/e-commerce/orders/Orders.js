import FusePageCarded from "@fuse/core/FusePageCarded";
import withReducer from "app/store/withReducer";
import { styled } from "@mui/material/styles";
import reducer from "../store";
import OrdersHeader from "./OrdersHeader";
import OrdersTable from "./OrdersTable";
import FusePageSimple from "@fuse/core/FusePageSimple";
import MapComp from "../mapComp/MapComp";
import AnnotatorWorkSpaceComp from "../annotatorWorkSpaceComp/AnnotatorWorkSpaceComp";
import QaWorkSpaceComp from "../qaWorkSpaceComp/QaWorkSpaceComp";
import { DataContext } from "../../../../../storeMain/context/data/DataContext";
import TableComp from "./tableComp/TableComp";
import styles from "./Orders.module.css";

const Root = styled(FusePageSimple)(({ theme }) => ({
  "& .FusePageSimple-header": {
    minHeight: 160,
    height: 160,
    [theme.breakpoints.up("lg")]: {
      marginRight: 12,
      borderBottomRightRadius: 20,
    },
  },
  "& .FusePageSimple-toolbar": {
    minHeight: 56,
    height: 56,
    alignItems: "flex-end",
  },
  "& .FusePageSimple-rightSidebar": {
    width: 288,
    border: 0,
    padding: "12px 0",
  },
  "& .FusePageSimple-content": {
    maxHeight: "100%",
    "& canvas": {
      maxHeight: "100%",
    },
  },
}));

const Orders1 = (props) => {
  return (
    <Root
      header={<OrdersHeader />}
      content={
        <div className="p-12 lg:ltr:pr-0 lg:rtl:pl-0">
          <div
            className={styles.btn}
            onClick={() => {
              props.backBtnHandler();
            }}
          >
            Back
          </div>
          <MapComp />
          {props.payLoadCheck.pointData &&
            props.ui.mapPage.projectTableComp.show && <TableComp />}
          {props.ui.mapPage.AnnotatorWorkSpaceComp.show &&
            props.data.userData.role === "Annotator" && (
              <AnnotatorWorkSpaceComp />
            )}
          {props.ui.mapPage.AnnotatorWorkSpaceComp.show &&
            props.data.userData.role === "QA" &&
            (props.data.downloadProject.currentStatus === "Upload Project" ||
            props.data.downloadProject.currentStatus === "Check QA" ||
            props.data.downloadProject.currentStatus === "Rejection" ||
            props.data.downloadProject.currentStatus === "Approved" ? (
              <QaWorkSpaceComp />
            ) : (
              <AnnotatorWorkSpaceComp />
            ))}
        </div>
      }
      innerScroll
    />
  );
};

const Orders = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <Orders1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

//export default AnnotatorWorkSpaceComp;

export default withReducer("eCommerceApp", reducer)(Orders);
