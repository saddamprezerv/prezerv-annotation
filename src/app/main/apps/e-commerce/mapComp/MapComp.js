import React from "react";
import styles from "./MapComp.module.css";
import { motion } from "framer-motion";
import MainViewer from "../mainViewer/MainViewer";

const MapComp = (props) => {
  const container = {
    show: {
      transition: {
        staggerChildren: 0.1,
      },
    },
  };
  return (
    <motion.div
      className="flex flex-wrap"
      variants={container}
      initial="hidden"
      animate="show"
    >
      <motion.div
        className="widget flex w-full p-10"
        // style={{ backgroundColor: "blue" }}
      >
        <MainViewer />
      </motion.div>
    </motion.div>
  );
};

export default MapComp;
