const viewerConfig = {
  center: [42.3601, -71.0589],
  zoom: 11,
  zoomControl: true,
  accessToken:
    "pk.eyJ1Ijoic2FkZGFtcWFpc2VyIiwiYSI6ImNrdHNwNnNpaTAwYTIydm1yYzd3enUyZjIifQ.MDpO9z8BL1BRvdrKfbul3Q",
  // baseLayer: "http://{s}.tile.osm.org/{z}/{x}/{y}.png",
  // baseLayer:
  //   "https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png",
  baseLayer: [
    // {
    //   id: 0,
    //   name: "Dark Canvas",
    //   type: "Other",
    //   url: "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png",
    // },
    {
      id: 0,
      name: "Dark Canvas",
      type: "Other",
      url: "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
      idMapBox: "mapbox/dark-v10",
    },
    {
      id: 1,
      name: "White Canvas",
      type: "Other",
      url: "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
      idMapBox: "mapbox/light-v10",
    },

    {
      id: 2,
      name: "Google Streets",
      type: "Google",
      url: "http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
      subdomains: ["mt0", "mt1", "mt2", "mt3"],
    },

    {
      id: 3,
      name: "World Imagery",
      type: "Other",
      url: "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
    },
    {
      id: 4,
      name: "Open Street Map",
      type: "Other",
      url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    },
    {
      id: 5,
      name: "Google Satellite Imagery",
      type: "Google",
      url: "http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
      subdomains: ["mt0", "mt1", "mt2", "mt3"],
    },
    {
      id: 6,
      name: "Google Hybrid",
      type: "Google",
      url: "http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}",
      subdomains: ["mt0", "mt1", "mt2", "mt3"],
    },
  ],
};

export default viewerConfig;
