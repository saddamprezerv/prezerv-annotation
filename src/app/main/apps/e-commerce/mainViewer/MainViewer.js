import React, { Component, createRef } from "react";
import styles from "./MainViewer.module.css";
import MapViewerConfig from "./configurations/mapViewerConfig";
import {
  tileLayer,
  map,
  geoJSON,
  control,
  layerGroup,
  LatLng,
  marker,
  circleMarker,
} from "leaflet";
import "leaflet/dist/leaflet.css";
import { DataContext } from "../../../../../storeMain/context/data/DataContext";
import MiniMapControl from "./minimap/Minimap";

const defaultColorObj = {
  fillColor: "#3589ff",
  weight: 1,
  opacity: 1,
  color: "#3589ff",
  // dashArray: "3",
  fillOpacity: 0.7,
};

const highLightColorObj = {
  fillColor: "#00FF00",
  weight: 1,
  color: "#00FF00",
  dashArray: "",
  fillOpacity: 0.5,
};
const hoverHighLightColorObj = {
  fillColor: "#d4af37",
  weight: 1,
  color: "#d4af37",
  dashArray: "",
  fillOpacity: 0.5,
};
const projectStatus = [
  {
    id: 0,
    name: "Not Started",
    color: "#C00000",
  },
  {
    id: 1,
    name: "start",
    color: "#4472C4",
  },
  {
    id: 2,
    name: "check_project",
    color: "#7030A0",
  },
  {
    id: 3,
    name: "upload_project",
    color: "#ED7D31",
  },
  {
    id: 4,
    name: "check_qa",
    color: "#E9FB05",
  },
  {
    id: 5,
    name: "rejection",
    color: "#FFC000",
  },
  {
    id: 5,
    name: "Rejected",
    color: "#FFC000",
  },
  {
    id: 6,
    name: "approved",
    color: "#00B050",
  },
  {
    id: 7,
    name: "Annotation",
    color: "#4472C4",
  },
  // {
  //   id: 8,
  //   name: null,
  //   color: "#E9FB05",
  // },
];
class MainViewer1 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  mainMapRef = createRef();
  initializeViewer = () => {
    this.props.setMapRef(this);
    const baseLayer = tileLayer(
      MapViewerConfig.baseLayer[this.props.mapParams.defaultBaseLayer].url,
      {
        tileSize: 512,
        zoomOffset: -1,
        accessToken: MapViewerConfig.accessToken,
        id: MapViewerConfig.baseLayer[1].idMapBox,
      }
    );

    this.map = map(this.mainMapRef.current, {
      center: MapViewerConfig.center,
      zoom: MapViewerConfig.zoom,

      maxZoom: 18,
      layers: [baseLayer],
    });

    this.props.initializeViewer(this.map, baseLayer);
  };
  changeBaseMapHandler = (i) => {
    if (this.props.mapParams.selectedBaseLayer) {
      this.props.map.removeLayer(this.props.mapParams.selectedBaseLayer);
    }

    const baseLayer =
      MapViewerConfig.baseLayer[i].type === "Google"
        ? tileLayer(MapViewerConfig.baseLayer[i].url, {
            subdomains: MapViewerConfig.baseLayer[i].subdomains,
            tileSize: 512,
            zoomOffset: -1,
          })
        : tileLayer(MapViewerConfig.baseLayer[i].url, {
            tileSize: 512,
            zoomOffset: -1,
            accessToken: MapViewerConfig.accessToken,
            id: MapViewerConfig.baseLayer[i].idMapBox,
          });

    this.props.addBaseMapSetStateHandler(baseLayer);
  };

  highlightFeatureHandler = (e) => {
    var layer = e.target;

    layer.setStyle(hoverHighLightColorObj);
    // let BdryNa = null;
    // if (this.props.ui.landingPage.clickedCard === "overDueTask") {
    //   BdryNa = e.target.feature.area_id;
    // }
    // if (this.props.ui.landingPage.clickedCard === "dueTask") {
    //   BdryNa = e.target.feature.area_id;
    // }
    let BdryNa = e.target.feature.area_id;

    //console.log(e.target);
    const StringBdryName = BdryNa.toString();

    //this.props.addJsonLabelSetStateHandler(BdryNa, layer);

    this.props.addJsonLabelSetStateHandler(StringBdryName, layer);
  };
  resetHighlightHandler = (e) => {
    var layer = e.target;
    //console.log("outttt", e.target);
    const status = projectStatus.find(
      ({ name }) => name === layer.feature.status
    );
    const tempObj = defaultColorObj;
    tempObj["fillColor"] = status.color;
    tempObj["color"] = status.color;
    layer.setStyle(tempObj);
    if (this.props.mapParams.gridPolyClickFeature) {
      const prevLayer = this.props.mapParams.gridPolyClickFeature;
      prevLayer.setStyle(highLightColorObj);
    }
    //info.update();
  };

  clickHighlightFeatureHandler = (e) => {
    var layer = e.target;
    if (this.props.mapParams.gridPolyClickFeature) {
      const prevLayer = this.props.mapParams.gridPolyClickFeature;
      const status = projectStatus.find(
        ({ name }) => name === prevLayer.feature.status
      );
      const tempObj = defaultColorObj;
      tempObj["fillColor"] = status.color;
      tempObj["color"] = status.color;
      prevLayer.setStyle(tempObj);
      // prevLayer.setStyle(defaultColorObj);
    }
    // if (this.props.ui.landingPage.clickedCard === "overDueTask") {
    //   this.props.getPointHandler(layer.feature.area_id);
    // }
    // if (this.props.ui.landingPage.clickedCard === "dueTask") {
    //   this.props.getPointHandler(layer.feature.id);
    // }
    this.props.getPointHandler(layer.feature.area_id);

    layer.setStyle(highLightColorObj);
    this.props.gridPolyClickFeatureHandler(layer);
  };
  clickToFeatureHandler = (e) => {
    //this.props.map.flyToBounds(e.target.getBounds());
    this.clickHighlightFeatureHandler(e);
  };
  onEachFeatureHandler = (feature, layer) => {
    layer.on({
      mouseover: this.highlightFeatureHandler,
      mouseout: this.resetHighlightHandler,
      click: this.clickToFeatureHandler,
    });
  };
  bindPopupToPolygon = (layer) => {
    layer
      .bindTooltip("Grid ID:" + this.props.mapParams.mapPolygonLabel, {
        permanent: false,
        direction: "auto",
      })
      .openTooltip();
  };

  styleFeatureHandler = (feature) => {
    console.log("Feature Property", feature);
    const status = projectStatus.find(({ name }) => name === feature.status);
    return {
      fillColor: status.color,
      weight: 1,
      opacity: 1,
      color: status.color,
      // dashArray: "3",
      fillOpacity: 0.7,
    };
    // return {
    //     fillColor: getColor(feature.properties.density),
    //     weight: 2,
    //     opacity: 1,
    //     color: 'white',
    //     dashArray: '3',
    //     fillOpacity: 0.7
    // };
  };
  addGridHandler = (geojsonFeature) => {
    if (this.props.mapParams.gridPoly) {
      this.props.map.removeLayer(this.props.mapParams.gridPoly);
    }
    console.log(geojsonFeature);
    const gridPoly = geoJSON(geojsonFeature, {
      onEachFeature: this.onEachFeatureHandler,
      style: this.styleFeatureHandler,

      // style: defaultColorObj,
    }).addTo(this.props.map);
    let gridPolyBounds = gridPoly.getBounds();

    this.props.map.flyToBounds(gridPolyBounds);
    this.props.gridPolyAddHandler(gridPoly);
    gridPoly.on("click", (event) => {
      console.log(event.sourceTarget.feature.id);
    });
  };

  displaySurveyPoints = (data) => {
    // console.log(data);
    if (this.props.mapParams.surveyPoints) {
      this.props.map.removeLayer(this.props.mapParams.surveyPoints);
    }
    var markerOptions = {
      fillColor: "#E25825",
      // weight: 2,
      opacity: 0.7,
      color: "#000000",
      fillOpacity: 1,
      radius: 5,
    };
    var markers = layerGroup();
    //const facilitiesList = data.payload.facilities;
    const surveyPointsList = data;
    //console.log(facilitiesList);
    for (var i = 0; i < surveyPointsList.length; i++) {
      //console.log(facilitiesList[i].latitude);

      let lat = parseFloat(surveyPointsList[i].coordinates[1]);
      let long = parseFloat(surveyPointsList[i].coordinates[0]);
      //console.log(lat);
      //console.log(lat);
      var surveyMarkers = circleMarker(new LatLng(lat, long), markerOptions);
      // var surveyMarkers = marker(new LatLng(lat, long), {
      //   icon: markerOptions,
      // });
      surveyMarkers.payLoad = surveyPointsList[i];
      markers.addLayer(surveyMarkers);
    }
    this.props.map.addLayer(markers);
    // this.props.map.flyToBounds(markers.getBounds);
    this.props.surveyPointsAddStateHandler(markers);
  };

  layerControl = () => {
    if (this.props.mapParams.mapLayerControl) {
      this.props.map.removeControl(this.props.mapParams.mapLayerControl);
    }
    var baseMaps = {};
    let overlayMaps = {
      "Grid Layer": this.props.mapParams.gridPoly,
    };
    if (this.props.mapParams.surveyPoints) {
      overlayMaps = {
        "Grid Layer": this.props.mapParams.gridPoly,
        "Survey Points": this.props.mapParams.surveyPoints,
      };
    }

    const layerControl = control
      .layers(baseMaps, overlayMaps)
      .addTo(this.props.map);
    this.props.addMapLayerControlStateHandler(layerControl);
  };

  componentDidMount() {
    this.initializeViewer();
  }
  render = () => {
    return (
      <div className={styles.main}>
        <div className={styles.map} ref={this.mainMapRef}>
          {/* <div className={styles.map} ref={this.mainMapRef}></div> */}
          <MiniMapControl
            handleBasemapSwitch={this.changeBaseMapHandler}
            minimapList={this.props.mapParams.defaultBaseLayer}
            defaultType={"light"}
            uiBottom={"25%"}
          />
        </div>
      </div>
    );
  };
}
const MainViewer = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <MainViewer1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default MainViewer;
