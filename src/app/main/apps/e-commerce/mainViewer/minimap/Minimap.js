import React, { useEffect, useRef, useState } from "react";
// import Img from "react-cool-img";
import mapboxDarkImg from "./utilities/mapbox-dark.jpg";
import mapboxLightImg from "./utilities/mapbox-light.jpg";
import mapboxHybridImg from "./utilities/mapbox-streets-satellite.jpg";
import mapboxStreetsImg from "./utilities/mapbox-streets.jpg";
import styles from "./Minimap.module.css";

const MiniMap = (props) => {
  const mainRef = useRef(null);
  const getMinimapList = (defaultType) => {
    let minimapList = [];
    let tempMinimapList = [
      <img title="Light" src={mapboxLightImg} alt="light" index={1} />,
      <img title="Streets" src={mapboxStreetsImg} alt="streets" index={2} />,
      <img
        title="Hybrid"
        src={mapboxHybridImg}
        alt="streetsSatellite"
        index={6}
      />,
      <img title="Dark" src={mapboxDarkImg} alt="dark" index={0} />,
    ];

    switch (defaultType) {
      case "streets":
        minimapList.push(tempMinimapList[1]);
        tempMinimapList = tempMinimapList.filter((_, idx) => idx !== 1);
        break;
      case "streetsSatellite":
        minimapList.push(tempMinimapList[2]);
        tempMinimapList = tempMinimapList.filter((_, idx) => idx !== 2);
        break;
      case "dark":
        minimapList.push(tempMinimapList[3]);
        tempMinimapList = tempMinimapList.filter((_, idx) => idx !== 3);
        break;

      default:
        minimapList.push(tempMinimapList[0]);
        tempMinimapList = tempMinimapList.filter((_, idx) => idx !== 0);
        break;
    }

    minimapList.push(...tempMinimapList);
    return minimapList;
  };

  const minimapListTmp = getMinimapList(props.defaultType);

  const [minimap, setMinimap] = useState(minimapListTmp[props.minimapList]);
  const [showMinimapsList, setShowMinimapsList] = useState(false);
  const [minimapList, setMinimapList] = useState(minimapListTmp);

  const setMiniMapImageFunc = (minimap) => {
    setMinimap(minimap);
    //console.log(minimap.props.index);
    props.handleBasemapSwitch(minimap.props.index);
  };

  const minimapToggleHandler = (e) => {
    if (e.target.alt !== minimap.props.alt) {
      const minimapList = getMinimapList(e.target.alt);
      setMinimapList(minimapList);
      setMiniMapImageFunc(minimapList[0]);
    }
    minimapsListToggleHandler();
  };

  const minimapsListToggleHandler = () => {
    minimapListOpenStateHandler(!showMinimapsList);
    setShowMinimapsList(!showMinimapsList);
  };

  useEffect(() => {
    document.addEventListener("mousedown", onMinimapPaneBlurHandler);
    return () => {
      document.removeEventListener("mousedown", onMinimapPaneBlurHandler);
    };
  }, []);

  const minimapListOpenStateHandler = (booled) => {
    if (props.minimapListOpenStateHandler) {
      props.minimapListOpenStateHandler(booled);
    }
  };

  const onMinimapPaneBlurHandler = (e) => {
    if (showMinimapsList) {
      if (!mainRef.contains(e.target)) {
        setShowMinimapsList(false);
        minimapListOpenStateHandler(false);
      }
    }
  };

  return (
    <>
      <div
        aria-hidden="true"
        className={styles.main}
        onClick={minimapsListToggleHandler}
      >
        {minimap}
        <span className={styles[minimap.props.title]}>
          {minimap.props.title}
        </span>
      </div>
      {showMinimapsList ? (
        <div
          ref={mainRef}
          aria-hidden="true"
          className={styles.mainFull}
          onClick={minimapToggleHandler}
        >
          <div>
            {minimapList[1]}
            {minimapList[2]}
          </div>
          <div>
            {minimapList[3]}
            {minimapList[0]}
          </div>
        </div>
      ) : null}
    </>
  );
};

export default MiniMap;
