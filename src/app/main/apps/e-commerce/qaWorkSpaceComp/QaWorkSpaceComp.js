import React, { useState } from "react";
import styles from "./QaWorkSpaceComp.module.css";
import { motion } from "framer-motion";
import { DataContext } from "../../../../../storeMain/context/data/DataContext";
import QaWorkSpace from "./qaWorkSpace/QaWorkSpace";

const QaWorkSpaceComp1 = (props) => {
  const [comment, setcomment] = useState("");
  const checkAnnotation = () => {
    window.open(props.data.downloadProject.url, "_blank");
    const data = {
      event_name: "check_qa",
      project_id: props.data.downloadProject.postProjStatus,
      user_id: props.data.userData.user_id,
      comment: comment,
      file: null,
      qa_id: props.data.userData.user_id,
    };
    props.postProjStatus(data);
    setcomment("");
  };

  // const rejectHandler = () => {
  //   // window.open(props.data.downloadProject.url, "_blank");
  //   const data = {
  //     event_name: "rejection",
  //     project_id: props.data.downloadProject.postProjStatus,
  //     user_id: props.data.userData.user_id,
  //     comment: comment,
  //     file: null,
  //     qa_id: props.data.userData.user_id,
  //   };
  //   props.postProjStatus(data);
  //   setcomment("");
  // };

  const approveHandler = () => {
    // window.open(props.data.downloadProject.url, "_blank");
    const data = {
      event_name: "approved",
      project_id: props.data.downloadProject.postProjStatus,
      user_id: props.data.userData.user_id,
      comment: comment,
      file: null,
      qa_id: props.data.userData.user_id,
    };
    props.postProjStatus(data);
    setcomment("");
  };

  const enterComment = (comment) => {
    //props.enterComment(comment);
    setcomment(comment);
  };
  const clickedSubmit = (fileContent) => {
    let data = null;

    let role = props.data.userData.role;
    const lowerCase = role.toLowerCase();
    console.log(lowerCase);
    data = {
      event_name: "rejection",
      project_id: props.data.downloadProject.postProjStatus,
      user_id: props.data.userData.user_id,
      comment: comment,
      file: fileContent,
      qa_id: props.data.userData.user_id,
      role: lowerCase,
    };
    if (fileContent) {
      props.uploadProjectApi(data);
      //alert("uploadProjectApi");
    } else {
      props.postProjStatus(data);
      //alert("postProjStatus");
    }
    setcomment("");
  };
  const container = {
    show: {
      transition: {
        staggerChildren: 0.1,
      },
    },
  };
  return (
    <motion.div
      className="flex flex-wrap"
      variants={container}
      initial="hidden"
      animate="show"
    >
      <motion.div
        className="widget flex w-full p-5"
        // style={{ backgroundColor: "blue" }}
      >
        <QaWorkSpace
          checkAnnotation={checkAnnotation}
          data={props.data}
          clickedSubmit={clickedSubmit}
          enterComment={enterComment}
          comment={comment}
          // rejectHandler={rejectHandler}
          approveHandler={approveHandler}
        />
      </motion.div>
    </motion.div>
  );
};

const QaWorkSpaceComp = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <QaWorkSpaceComp1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default QaWorkSpaceComp;
