import React from "react";
import styles from "./AnnotatorWorkSpace.module.css";
import Grid from "@mui/material/Grid";
import { useFilePicker } from "use-file-picker";

const AnnotatorWorkSpace = (props) => {
  const handleFieldDate = (event) => {
    // console.log(event.target.value);
    console.log("my currents tatus", props.data.downloadProject.currentStatus);
    props.enterComment(event.target.value);
  };
  const [
    openFileSelector,
    { filesContent, loading, errors, plainFiles, clear },
  ] = useFilePicker({
    multiple: false,
    accept:
      props.data.downloadProject.currentStatus === "Check Project"
        ? ".zip"
        : ".txt",
  });
  const projectStatus = props.data.projectStatus;

  const submitHandler = () => {
    console.log("plainFiles", plainFiles);
    props.clickedSubmit(plainFiles[0]);
  };

  const status = projectStatus.find(
    ({ name }) => name === props.data.downloadProject.currentStatus
  );
  return (
    <div className={styles.main}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={1}
      >
        <Grid item md={2}>
          <div className={styles.contentDiv}>
            <div className={styles.content}>
              <div className={styles.heading}>Area #</div>
              <div className={styles.value}>{props.data.selectedGridId}</div>
            </div>
          </div>
        </Grid>
        <Grid item md={2}>
          <div className={styles.contentDiv}>
            <div className={styles.content}>
              <div className={styles.heading}>Status</div>
              <div
                className={styles.btnDiv}
                style={{ backgroundColor: status.color }}
              >
                {status.name}
              </div>
            </div>
          </div>
        </Grid>
        <Grid item md={2}>
          <div className={styles.contentDiv}>
            <div className={styles.content}>
              <div
                className={
                  props.data.downloadProject.currentStatus === "Not Started"
                    ? styles.btnDivBig
                    : styles.btnDivBigDisable
                }
                onClick={() => {
                  if (
                    props.data.downloadProject.currentStatus === "Not Started"
                  ) {
                    props.clickedStart();
                  }
                }}
              >
                Start Annotation
              </div>
            </div>
          </div>
        </Grid>
        <Grid item md={2}>
          <div className={styles.contentDiv}>
            <div className={styles.content}>
              <div
                className={
                  props.data.downloadProject.currentStatus === "Annotation" ||
                  props.data.downloadProject.currentStatus === "Rejection"
                    ? styles.btnDivBig
                    : styles.btnDivBigDisable
                }
                onClick={() => {
                  if (
                    props.data.downloadProject.currentStatus === "Annotation" ||
                    props.data.downloadProject.currentStatus === "Rejection"
                  ) {
                    openFileSelector();
                  }
                }}
              >
                Check Project File
              </div>
            </div>
          </div>
        </Grid>
        <Grid item md={2}>
          <div className={styles.contentDiv}>
            <div className={styles.content}>
              <div
                className={
                  props.data.downloadProject.currentStatus === "Check Project"
                    ? styles.btnDivBig
                    : styles.btnDivBigDisable
                }
                onClick={() => {
                  if (
                    props.data.downloadProject.currentStatus === "Check Project"
                  ) {
                    openFileSelector();
                  }
                }}
              >
                Upload Project Zip
              </div>
            </div>
          </div>
        </Grid>
        {props.data.downloadProject.currentStatus === "Rejection" && (
          <Grid item md={12}>
            <div className={styles.contentDiv}>
              <div className={styles.content}>
                <div
                  className={styles.btnDivBig}
                  onClick={() => {
                    props.clickedRejectedFile();
                  }}
                  style={{ padding: "10px" }}
                >
                  Download Rejected File
                </div>
              </div>
            </div>
          </Grid>
        )}
      </Grid>
      {/* <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={1}
      >
        <Grid item md={10}>
          {filesContent.map((file, index) => (
            <div key={"fileName" + index}>
              <div className={styles.fileName}>{file.name}</div>
            </div>
          ))}
        </Grid>
      </Grid> */}

      <div className={styles.commentSection}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={1}
        >
          <Grid item md={2}>
            <div className={styles.contentDiv}>
              {filesContent.map((file, index) => (
                <div key={"fileName" + index}>
                  <div
                    className={styles.fileName}
                    style={{ fontWeight: "700" }}
                  >
                    File Name
                  </div>
                  <div className={styles.fileName}>{file.name}</div>
                </div>
              ))}
            </div>
          </Grid>

          <Grid item md={8}>
            <div className={styles.contentDiv}>
              <div className={styles.TextBoxDiv}>
                <input
                  type="text"
                  id="comment"
                  name="comment"
                  placeholder="Enter Comment"
                  style={{ width: "100%", height: "30px" }}
                  onChange={handleFieldDate}
                  value={props.comment}
                  // defaultValue={props.data.downloadProject.comment}
                />
              </div>
            </div>
          </Grid>
          <Grid item md={2}>
            <div className={styles.contentDiv}>
              <div
                className={
                  props.data.downloadProject.currentStatus === "Annotation" ||
                  props.data.downloadProject.currentStatus ===
                    "Check Project" ||
                  props.data.downloadProject.currentStatus === "Rejection"
                    ? styles.SubmitBtnDiv
                    : styles.SubmitBtnDivDisable
                }
                onClick={() => {
                  if (
                    props.data.downloadProject.currentStatus === "Annotation" ||
                    props.data.downloadProject.currentStatus ===
                      "Check Project" ||
                    props.data.downloadProject.currentStatus === "Rejection"
                  ) {
                    submitHandler();
                  }
                }}
              >
                Submit
              </div>
            </div>
          </Grid>
        </Grid>
        {props.data.historyData.length > 0 && (
          <>
            <Grid container direction="row" spacing={1}>
              <div className={styles.headingHistory}>Project History</div>
            </Grid>

            <Grid container direction="row" spacing={1}>
              <div className={styles.flexConatiner}>
                <div className={styles.headingRow}>Area ID</div>
                <div className={styles.headingRow}>Project ID</div>
                <div className={styles.headingRow}>User Name</div>
                <div className={styles.headingRow}>User Role</div>
                <div className={styles.headingRow}>Event</div>
                <div className={styles.headingRow}>Prevoius Event</div>
                <div className={styles.headingRow}>Comment</div>
              </div>
            </Grid>
            {props.data.historyData.map((k, i) => {
              return (
                <Grid
                  container
                  direction="row"
                  spacing={1}
                  key={"history" + k.event + i}
                >
                  <div className={styles.flexConatiner}>
                    <div className={styles.headingValue}>{k.area_id}</div>
                    <div className={styles.headingValue}>{k.project_id}</div>
                    <div className={styles.headingValue}>{k.user_name}</div>
                    <div className={styles.headingValue}>{k.user_role}</div>
                    <div className={styles.headingValue}>{k.event}</div>
                    <div className={styles.headingValue}>{k.prev_event} </div>
                    <div className={styles.headingValue}>{k.comment}</div>
                  </div>
                </Grid>
              );
            })}
          </>
        )}
      </div>
    </div>
  );
};

export default AnnotatorWorkSpace;
