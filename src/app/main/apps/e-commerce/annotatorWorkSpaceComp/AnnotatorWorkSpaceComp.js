import React, { useState } from "react";
import styles from "./AnnotatorWorkSpaceComp.module.css";
import { motion } from "framer-motion";
import { DataContext } from "../../../../../storeMain/context/data/DataContext";
import AnnotatorWorkSpace from "./annotatorWorkSpace/AnnotatorWorkSpace";

const AnnotatorWorkSpaceComp1 = (props) => {
  const [comment, setcomment] = useState("");

  const clickedRejectedFile = () => {
    window.open(props.data.downloadProject.url, "_blank");
  };
  const clickedStart = () => {
    window.open(props.data.downloadProject.url, "_blank");
    const data = {
      event_name: "start",
      project_id: props.data.downloadProject.postProjStatus,
      user_id: props.data.userData.user_id,
      comment: comment,
      file: null,
      qa_id: props.data.userData.user_id,
    };
    props.postProjStatus(data);
    setcomment("");
  };

  const enterComment = (comment) => {
    //props.enterComment(comment);
    setcomment(comment);
  };
  const clickedSubmit = (fileContent) => {
    let data = null;
    if (props.data.downloadProject.currentStatus === "Check Project") {
      let role = props.data.userData.role;
      const lowerCase = role.toLowerCase();
      console.log(lowerCase);
      data = {
        event_name: "upload_project",
        project_id: props.data.downloadProject.postProjStatus,
        user_id: props.data.userData.user_id,
        comment: comment,
        file: fileContent,
        qa_id: props.data.userData.user_id,
        role: lowerCase,
      };

      // data = {
      //   file: fileContent,
      //   project_id: props.data.downloadProject.postProjStatus,
      //   role: lowerCase,
      // };
      props.uploadProjectApi(data);
    } else {
      data = {
        event_name: "check_project",
        project_id: props.data.downloadProject.postProjStatus,
        user_id: props.data.userData.user_id,
        comment: comment,
        file: fileContent,
        qa_id: props.data.userData.user_id,
      };
      props.postProjStatus(data);
    }

    setcomment("");
  };
  const container = {
    show: {
      transition: {
        staggerChildren: 0.1,
      },
    },
  };
  return (
    <motion.div
      className="flex flex-wrap"
      variants={container}
      initial="hidden"
      animate="show"
    >
      <motion.div
        className="widget flex w-full p-5"
        // style={{ backgroundColor: "blue" }}
      >
        <AnnotatorWorkSpace
          clickedStart={clickedStart}
          data={props.data}
          clickedSubmit={clickedSubmit}
          enterComment={enterComment}
          comment={comment}
          clickedRejectedFile={clickedRejectedFile}
        />
      </motion.div>
    </motion.div>
  );
};

const AnnotatorWorkSpaceComp = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <AnnotatorWorkSpaceComp1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default AnnotatorWorkSpaceComp;
