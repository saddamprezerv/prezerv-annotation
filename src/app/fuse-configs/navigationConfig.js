import { authRoles } from "app/auth";
import i18next from "i18next";
import DocumentationNavigation from "../main/documentation/DocumentationNavigation";

import ar from "./navigation-i18n/ar";
import en from "./navigation-i18n/en";
import tr from "./navigation-i18n/tr";

i18next.addResourceBundle("en", "navigation", en);
i18next.addResourceBundle("tr", "navigation", tr);
i18next.addResourceBundle("ar", "navigation", ar);

const navigationConfig = [
  {
    id: "applications",
    title: "",
    translate: "APPLICATIONS",
    type: "group",
    icon: "apps",
    children: [
      {
        id: "dashboards",
        title: "Dashboards",
        translate: "Annotators",
        type: "collapse",
        icon: "dashboard",
        children: [
          {
            id: "project-dashboard",
            title: "Get Started",
            type: "item",
            url: "/apps/dashboards/project",
          },
        ],
      },
    ],
  },
  // {
  //   id: "pages",
  //   title: "Pages",
  //   type: "group",
  //   icon: "pages",
  //   children: [
  //     {
  //       id: "authentication",
  //       title: "Authentication",
  //       type: "collapse",
  //       icon: "lock",

  //       children: [
  //         {
  //           id: "login-v2",
  //           title: "Login v2",
  //           type: "item",
  //           url: "/pages/auth/login-2",
  //         },
  //       ],
  //     },
  //   ],
  // },
];

export default navigationConfig;
