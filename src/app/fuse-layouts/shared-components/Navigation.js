import FuseNavigation from "@fuse/core/FuseNavigation";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import clsx from "clsx";
import { memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectNavigation } from "app/store/fuse/navigationSlice";
import { navbarCloseMobile } from "../../store/fuse/navbarSlice";
import { DataContext } from "../../../storeMain/context/data/DataContext";

const Navigation1 = (props) => {
  const navigation = useSelector(selectNavigation);
  console.log("navigation", navigation);
  let role = props.data.userData.role;
  let title = null;
  if (role === "QA") {
    title = "Quality Control";
    navigation[0].children[0].title = title;
  }
  if (role === "Annotator") {
    title = "Annotator";
    navigation[0].children[0].title = title;
  }
  const theme = useTheme();
  const mdDown = useMediaQuery(theme.breakpoints.down("lg"));
  const dispatch = useDispatch();

  function handleItemClick(item) {
    if (mdDown) {
      dispatch(navbarCloseMobile());
    }
  }

  return (
    <FuseNavigation
      className={clsx("navigation", props.className)}
      navigation={navigation}
      layout={props.layout}
      dense={props.dense}
      active={props.active}
      onItemClick={handleItemClick}
    />
  );
};

Navigation1.defaultProps = {
  layout: "vertical",
};

const Navigation = (props) => (
  <DataContext.Consumer>
    {(mainConsumer) => <Navigation1 {...mainConsumer} {...props} />}
  </DataContext.Consumer>
);

export default memo(Navigation);
