import React, { Component } from "react";
import dbAxios from "../../services/axios/PgAxios";
import ApiUrls from "../../services/configurations/ApiConfig";
const DataContext = React.createContext();

class DataProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      map: null,
      mapParams: {
        mapLayerControl: null,
        surveyPoints: null,
        selectedBaseLayer: null,
        defaultBaseLayer: 0,
        gridPoly: null,
        gridPolyClickFeature: null,
        mapPolygonLabel: "",
      },
      data: {
        projectStatus: [
          {
            id: 0,
            name: "Not Started",
            color: "#C00000",
          },
          {
            id: 1,
            name: "Annotation",
            color: "#4472C4",
          },
          {
            id: 2,
            name: "Check Project",
            color: "#7030A0",
          },
          {
            id: 3,
            name: "Upload Project",
            color: "#ED7D31",
          },
          {
            id: 4,
            name: "Check QA",
            color: "#E9FB05",
          },
          {
            id: 5,
            name: "Rejection",
            color: "#FFC000",
          },
          {
            id: 6,
            name: "Approved",
            color: "#00B050",
          },
        ],
        gridData: null,
        gridCountTasks: null,
        gridOverDue: [],
        gridCountOverDue: null,
        gridQa: [],
        gridCountQa: null,
        pointData: null,
        areaProjects: null,
        selectedGridId: null,
        userData: null,
        historyData: [],
        downloadProject: {
          url: null,
          currentStatus: null,
          projId: null,
          eventName: null,
        },
      },
      payLoadCheck: {
        pointData: false,
        gridData: false,
        gridOverDue: false,
        gridQa: false,
      },

      ui: {
        landingPage: {
          clickedCard: null,
        },
        mapPage: {
          show: false,
          AnnotatorWorkSpaceComp: {
            show: false,
          },
          projectTableComp: {
            show: false,
          },
        },
        loginPage: {
          show: true,
          login: true,
          loggedIn: false,
        },
      },
      setMapRef: (ref) => {
        this.mapRef = ref;
      },
      initializeViewer: (ref, baseLayer) => {
        this.setState(
          {
            map: ref,
            mapParams: {
              ...this.state.mapParams,
              selectedBaseLayer: baseLayer,
            },
          },
          async () => {
            //this.state.DataContextInitHandler();
            if (this.state.ui.landingPage.clickedCard === "dueTask") {
              this.mapRef.addGridHandler(this.state.data.gridData);
            }
            if (this.state.ui.landingPage.clickedCard === "dueTaskQA") {
              this.mapRef.addGridHandler(this.state.data.gridData);
            }
            if (
              this.state.ui.landingPage.clickedCard === "overDueTask" &&
              this.state.data.gridOverDue.length > 0
            ) {
              this.mapRef.addGridHandler(this.state.data.gridOverDue);
            }
            if (
              this.state.ui.landingPage.clickedCard === "qaTask" &&
              this.state.data.gridQa.length > 0
            ) {
              this.mapRef.addGridHandler(this.state.data.gridQa);
            }

            //console.log("Grid DATA", this.state.data.gridData);
          }
        );
      },
      addBaseMapSetStateHandler: (baseLayer) => {
        this.setState(
          {
            mapParams: {
              ...this.state.mapParams,
              selectedBaseLayer: baseLayer,
            },
          },
          () => {
            this.state.map.addLayer(this.state.mapParams.selectedBaseLayer);
          }
        );
      },
      addMapLayerControlStateHandler: (param) => {
        this.setState(
          {
            mapParams: {
              ...this.state.mapParams,
              mapLayerControl: param,
            },
          },
          () => {
            //this.state.map.addLayer(this.state.mapParams.selectedBaseLayer);
          }
        );
      },
      addJsonLabelSetStateHandler: (BdryNa, layer) => {
        this.setState(
          {
            mapParams: {
              ...this.state.mapParams,
              mapPolygonLabel: BdryNa,
            },
          },
          () => {
            this.mapRef.bindPopupToPolygon(layer);
          }
        );
      },
      DataContextInitHandler: async () => {
        await this.state.getGridHandler();
        await this.state.getGridOverDueHandler();
      },
      taskClickHandler: (mapShow, clickedCard) => {
        this.setState({
          ui: {
            ...this.state.ui,
            landingPage: {
              ...this.state.ui.landingPage,
              clickedCard: clickedCard,
            },
            mapPage: {
              ...this.state.ui.mapPage,
              show: mapShow,
            },
          },
        });
      },
      backBtnHandler: () => {
        this.setState(
          {
            ui: {
              ...this.state.ui,

              mapPage: {
                ...this.state.ui.mapPage,
                show: false,
              },
            },
          },
          () => {
            this.state.DataContextInitHandler();
          }
        );
      },
      gridPolyAddHandler: (param) => {
        this.setState(
          {
            mapParams: {
              ...this.state.mapParams,
              gridPoly: param,
            },
          },
          () => {
            console.log("grid Poly Add", this.state.mapParams.gridPoly);
            this.mapRef.layerControl();
          }
        );
      },
      gridPolyClickFeatureHandler: (param) => {
        this.setState(
          {
            mapParams: {
              ...this.state.mapParams,
              gridPolyClickFeature: param,
            },
          },
          () => {
            // console.log("grid Poly Add", this.state.mapParams.gridPoly);
          }
        );
      },

      surveyPointsAddStateHandler: (param) => {
        this.setState(
          {
            mapParams: {
              ...this.state.mapParams,
              surveyPoints: param,
            },
          },
          () => {
            // console.log("grid Poly Add", this.state.mapParams.gridPoly);
            this.mapRef.layerControl();
          }
        );
      },

      //API
      loginUserApi: async (data) => {
        // const data = {
        //   username: "sadam",
        //   password: "pakistan123",
        // };
        let form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }
        await dbAxios
          .post(ApiUrls.user.loginUser.url, form_data)
          .then(async (res) => {
            if (res.msg === "Login Success") {
              console.log("Login", res);
              sessionStorage.setItem("token", res.access);
              sessionStorage.setItem("refresh", res.refresh);
              sessionStorage.setItem("user", data.username);
              sessionStorage.setItem("role", res.role);
              sessionStorage.setItem("userid", res.user_id);
              sessionStorage.setItem("email", res.email);
              sessionStorage.setItem("password", data.password);

              console.log("Responce Annotation Api", res);
              //await this.state.dataContextTriggerHandler();
              this.setState(
                {
                  ui: {
                    ...this.state.ui,
                    loginPage: {
                      ...this.state.ui.loginPage,
                      show: false,
                      login: true,
                      loggedIn: true,
                    },
                  },
                  data: {
                    ...this.state.data,
                    userData: {
                      token: res.access,
                      refresh: res.refresh,
                      user: data.username,
                      user_id: res.user_id,
                      password: data.password,
                      role: res.role,
                      email: res.email,
                    },
                  },
                },
                async () => {
                  await this.state.setDefaultAxiousHeaders();
                  await this.state.DataContextInitHandler();
                }
              );
            }
          })
          .catch((err) => {
            console.log(err);
            this.setState(
              {
                ui: {
                  ...this.state.ui,
                  loginPage: {
                    ...this.state.ui.loginPage,
                    show: true,
                    login: false,
                  },
                },
              },
              () => {}
            );
          });
      },
      setDefaultAxiousHeaders: () => {
        dbAxios.defaults.headers.common["Authorization"] =
          "Bearer " + sessionStorage.getItem("token");

        dbAxios.defaults.headers.post["Content-Type"] = "multipart/form-data";
        //dbAxios.defaults.headers.get["Content-Type"] = "multipart/form-data";
        const userData = this.state.data.userData;
        const token = sessionStorage.getItem("token");
        const refresh = sessionStorage.getItem("refresh");
        const user = sessionStorage.getItem("user");
        const role = sessionStorage.getItem("role");
        const user_id = sessionStorage.getItem("userid");
        const password = sessionStorage.getItem("password");
        const email = sessionStorage.getItem("email");
        if (userData === null) {
          this.setState(
            {
              data: {
                ...this.state.data,
                userData: {
                  token: token,
                  refresh: refresh,
                  user: user,
                  user_id: user_id,
                  password: password,
                  role: role,
                  email: email,
                },
              },
              ui: {
                ...this.state.ui,
                loginPage: {
                  ...this.state.ui.loginPage,
                  loggedIn: true,
                },
              },
            },
            () => {
              console.log("userData", this.state.data.userData);
              this.state.DataContextInitHandler();
            }
          );
        }
      },
      getGridHandler: async () => {
        //console.log("in feting api");
        await dbAxios
          .get(ApiUrls.features.grid.url)
          .then(async (res) => {
            console.log("grid response", res);
            const gridCount = [];
            const tempData = res;

            tempData.forEach((k) => {
              k["type"] = "Feature";
              k["properties"] = {};
              if (k.status === "Not Started") {
                gridCount.push(k);
              }
            });
            //console.log("Not Satrted Count", gridCount.length, gridCount);
            this.setState(
              {
                data: {
                  ...this.state.data,
                  gridData: tempData,
                  gridCountTasks: gridCount.length,
                },
                payLoadCheck: {
                  ...this.state.payLoadCheck,
                  gridData: true,
                },
              },
              () => {
                if (this.state.ui.mapPage.show) {
                  this.mapRef.addGridHandler(this.state.data.gridData);
                }
                // this.mapRef.addGridHandler(this.state.data.gridData);
                // console.log("Grid DATA", this.state.data.gridData);
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
      },
      getGridOverDueHandler: async () => {
        console.log("IDDDDD", this.state.data.userData.user_id);
        const data = {
          user_id: this.state.data.userData.user_id,
          role: this.state.data.userData.role,
        };
        let form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }
        console.log("form_data", this.state.data.userData.user_id);
        await dbAxios
          .post(ApiUrls.features.gridOverDue.url, form_data)
          .then(async (res) => {
            console.log("gridOverDue response", res);

            const gridOverDue = res.overdue_grids;

            gridOverDue.forEach((k) => {
              k["type"] = "Feature";
              k["properties"] = {};
            });

            const gridQa = res.qa_grids;

            gridQa.forEach((k) => {
              k["type"] = "Feature";
              k["properties"] = {};
            });
            //console.log("Not Satrted Count", gridCount.length, gridCount);
            this.setState(
              {
                data: {
                  ...this.state.data,
                  gridOverDue: gridOverDue,
                  gridCountOverDue: res.overdue_count,
                  gridQa: gridQa,
                  gridCountQa: res.qa_count,
                },
                payLoadCheck: {
                  ...this.state.payLoadCheck,
                  gridOverDue: true,
                  gridQa: true,
                },
              },
              () => {
                if (this.state.ui.mapPage.show) {
                  if (this.state.ui.landingPage.clickedCard === "dueTask") {
                    this.mapRef.addGridHandler(this.state.data.gridData);
                  }

                  if (
                    this.state.ui.landingPage.clickedCard === "overDueTask" &&
                    this.state.data.gridOverDue.length > 0
                  ) {
                    this.mapRef.addGridHandler(this.state.data.gridOverDue);
                  }
                  if (
                    this.state.ui.landingPage.clickedCard === "qaTask" &&
                    this.state.data.gridQa.length > 0
                  ) {
                    this.mapRef.addGridHandler(this.state.data.gridQa);
                  }
                }
                // this.mapRef.addGridHandler(this.state.data.gridData);
                console.log("Grid DATA Over Due", this.state.data.gridOverDue);
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
      },
      getPointHandler: async (id) => {
        await dbAxios
          .get(ApiUrls.features.point.url + id)
          .then(async (res) => {
            console.log("point response", res);
            const surveyPoints = res.area_points;
            const areaProjects = res.area_projects;
            this.mapRef.displaySurveyPoints(surveyPoints);

            this.setState(
              {
                data: {
                  ...this.state.data,
                  pointData: surveyPoints,
                  areaProjects: areaProjects,
                  selectedGridId: id,
                },
                payLoadCheck: {
                  ...this.state.payLoadCheck,
                  pointData: true,
                },
                ui: {
                  ...this.state.ui,

                  mapPage: {
                    ...this.state.ui.mapPage,
                    AnnotatorWorkSpaceComp: {
                      ...this.state.ui.mapPage.AnnotatorWorkSpaceComp,
                      show: false,
                    },

                    projectTableComp: {
                      ...this.state.ui.mapPage.projectTableComp,
                      show: true,
                    },
                  },
                },
              },
              () => {
                //this.mapRef.addGridHandler(this.state.data.gridData);
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
      },
      getProjectDownload: async (projId, userId, eventName, currentStatus) => {
        this.setState(
          {
            ui: {
              ...this.state.ui,

              mapPage: {
                ...this.state.ui.mapPage,
                AnnotatorWorkSpaceComp: {
                  ...this.state.ui.mapPage.AnnotatorWorkSpaceComp,
                  show: true,
                },
                projectTableComp: {
                  ...this.state.ui.mapPage.projectTableComp,
                  show: false,
                },
              },
            },
            data: {
              ...this.state.data,
              downloadProject: {
                ...this.state.data.downloadProject,
                currentStatus: currentStatus,
                postProjStatus: projId,
                eventName: eventName,
              },
            },
          },
          () => {}
        );
        //Temporary
        // if (
        //   currentStatus === "Upload Project" ||
        //   currentStatus === "Check QA" ||
        //   currentStatus === "Rejection" ||
        //   currentStatus === "Approved"
        // ) {
        //   this.setState(
        //     {
        //       ui: {
        //         ...this.state.ui,

        //         mapPage: {
        //           ...this.state.ui.mapPage,
        //           AnnotatorWorkSpaceComp: {
        //             ...this.state.ui.mapPage.AnnotatorWorkSpaceComp,
        //             show: true,
        //           },
        //           projectTableComp: {
        //             ...this.state.ui.mapPage.projectTableComp,
        //             show: false,
        //           },
        //         },
        //       },
        //       data: {
        //         ...this.state.data,
        //         downloadProject: {
        //           ...this.state.data.downloadProject,
        //           url: "www.google.com",
        //           currentStatus: currentStatus,
        //           postProjStatus: projId,
        //           eventName: eventName,
        //         },
        //       },
        //     },
        //     () => {

        //     }
        //   );
        // }
        await dbAxios
          .get(
            ApiUrls.download.url +
              "project_id=" +
              projId +
              "&user_id=" +
              userId +
              "&event_name=" +
              eventName
          )
          .then(async (res) => {
            console.log("project download response", res);
            this.setState(
              {
                // ui: {
                //   ...this.state.ui,

                //   mapPage: {
                //     ...this.state.ui.mapPage,
                //     AnnotatorWorkSpaceComp: {
                //       ...this.state.ui.mapPage.AnnotatorWorkSpaceComp,
                //       show: true,
                //     },
                //     projectTableComp: {
                //       ...this.state.ui.mapPage.projectTableComp,
                //       show: false,
                //     },
                //   },
                // },s
                data: {
                  ...this.state.data,
                  downloadProject: {
                    ...this.state.data.downloadProject,
                    url: res.URL,
                  },
                },
              },
              () => {
                //console.log(props.data.downloadProject.currentStatus);
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
      },
      postProjStatus: async (data) => {
        // const data = {
        //   username: "sadam",
        //   password: "pakistan123",
        // };
        console.log("Post Status Api Data", data);
        let form_data = new FormData();
        for (var key in data) {
          if (key === "file") {
            form_data.append(key, data[key]);
          } else {
            form_data.append(key, data[key]);
          }
        }
        let currentStatus = null;
        if (data.event_name === "start") {
          currentStatus = "Annotation";
        } else if (data.event_name === "check_project") {
          currentStatus = "Check Project";
        } else if (data.event_name === "upload_project") {
          currentStatus = "Upload Project";
        } else if (data.event_name === "check_qa") {
          currentStatus = "Check QA";
        } else if (data.event_name === "rejection") {
          currentStatus = "Rejection";
        } else if (data.event_name === "approved") {
          currentStatus = "Approved";
        }
        console.log(currentStatus);
        await dbAxios
          .post(ApiUrls.status.url, form_data)
          .then(async (res) => {
            console.log("Post Status Api", res);
            this.setState(
              {
                data: {
                  ...this.state.data,
                  downloadProject: {
                    ...this.state.data.downloadProject,
                    currentStatus: currentStatus,
                  },
                },
              },
              async () => {
                //console.log(this.state.data.downloadProject.url);
                await this.state.getHistory();

                if (
                  this.state.ui.landingPage.clickedCard === "overDueTask" ||
                  this.state.ui.landingPage.clickedCard === "dueTask"
                ) {
                  await this.state.getGridOverDueHandler();
                }
                if (this.state.ui.landingPage.clickedCard === "dueTask") {
                  await this.state.getGridHandler();
                }
                if (this.state.ui.landingPage.clickedCard === "dueTaskQA") {
                  await this.state.getGridHandler();
                }
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
      },
      getHistory: async () => {
        await dbAxios
          .get(ApiUrls.history.url + this.state.data.selectedGridId)
          .then(async (res) => {
            console.log("History", res);
            this.setState(
              {
                data: {
                  ...this.state.data,
                  historyData: res,
                },
              },
              () => {
                //console.log(this.state.data.downloadProject.url);
              }
            );
          })
          .catch((err) => {
            console.log(err);
          });
      },
      logOutUserApi: async (data) => {
        // const data = {
        //   username: "sadam",
        //   password: "pakistan123",
        // };
        let form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }
        await dbAxios
          .post(ApiUrls.user.logOutUser.url, form_data)
          .then(async (res) => {
            if (res.msg === "Successfully Logged out") {
              console.log("Login", "here at logout");
              sessionStorage.clear();

              this.setState(
                {
                  ui: {
                    ...this.state.ui,
                    loginPage: {
                      ...this.state.ui.loginPage,
                      loggedIn: false,
                    },
                  },
                },
                async () => {
                  console.log(this.state.ui.loginPage.loggedIn);
                }
              );
            }
          })
          .catch((err) => {
            console.log(err);
          });
      },
      uploadProjectApi: async (data) => {
        console.log(data);
        let form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }
        await dbAxios
          .post(ApiUrls.upload.url, form_data)
          .then(async (res) => {
            console.log("Uploaded Project File");

            this.state.postProjStatus(data);
          })
          .catch((err) => {
            console.log(err);
          });
      },
    };
  }

  componentDidMount() {
    //this.state.loginUserApi();
    //this.state.DataContextInitHandler();
  }

  render = () => {
    return (
      <DataContext.Provider value={this.state}>
        {this.props.children}
      </DataContext.Provider>
    );
  };
}

export { DataContext };

export default DataProvider;
