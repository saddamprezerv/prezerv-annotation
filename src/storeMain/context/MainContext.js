import React from "react";
import DataProvider from "./data/DataContext";

const dataProvider = (props) => <DataProvider>{props.children}</DataProvider>;

export default dataProvider;
