const ApiUrls = {
  user: {
    loginUser: {
      url: "api/user/login",
    },
    logOutUser: {
      url: "api/user/logout",
    },
    registerUser: {
      url: "api/user/register",
    },
  },
  download: { url: "project/download?" },
  status: { url: "project/status" },
  upload: { url: "uploadfile" },
  history: { url: "history?area_id=" },
  features: {
    grid: { url: "api/grid" },
    gridOverDue: { url: "api/grid-overdue" },
    point: { url: "api/points?area_id=" },
  },
};

export default ApiUrls;
