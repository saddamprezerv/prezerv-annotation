import axios from "axios";
import { pgAxiousConfig } from "../configurations/Config";

const dbAxios = axios.create({
  baseURL: `http://${pgAxiousConfig.ip}/`,
});

dbAxios.interceptors.response.use(
  (response) => {
    // Do something with response data
    if (response.data.errors) {
      return Promise.reject(response.data.errors);
    } else {
      return response.data;
    }
  },

  (error) => {
    // Do something with response error
    return Promise.reject(error);
  }
);
export default dbAxios;
